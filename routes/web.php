<?php

use App\Http\Controllers\AboutController;
use App\Http\Controllers\AddController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PrimerController;


Route::get('/', function () {
    return view('welcome');
});

Route::get('/hola', function () {
    return "prueba funcionamiento";
});

Route::get('users/{id}/{name}', function ($id, $name) {
    return 'Bienvenido usuario:'  . $id . " esta es tu cuenta " . $name;
})->where('id', '[0-9]+');


// es la llamada desde un controlador
Route::get('/primer', [PrimerController::class, 'saludo']);

// llamada a COntrolador con funciones pre definidas


Route::get('/add', [AddController::class, 'create']);
//Route::resource('add', AddController::class);


// llamada a una vista
Route::view('/contactos', 'contact');

//llamada a al controlador que maneja la vista
Route::get('/about', [AboutController::class, 'about']);
