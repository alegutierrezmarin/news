<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Primera pagina de Pruebas</title>
</head>

<body>
    @section('sidebar')

        <p>Seccion parent/ Master page</p>
    @show

    <div>
        @yield('component')
    </div>


</body>

</html>
